import java.lang.System;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int ram_size = 256;
        int page_size = 32;
        int number_of_disks = 2;

        try {
            System.out.println("How much RAM memory is there on the simulated computer?");
            ram_size = keyboard.nextInt();
            System.out.println("What is the size of a page/frame?");
            page_size = keyboard.nextInt();
            System.out.println("How many hard disks the simulated computer has?");
            number_of_disks = keyboard.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("ERROR! Only integer values allowed.");
            System.out.println("Starting with default settings: RAM = 100, PAGE = 10, DISKS = 2");
        }
        keyboard.nextLine();

        //Computer computer = new Computer();
        Computer computer = new Computer(ram_size, page_size, number_of_disks);

        System.out.println("Simulation started:");
        String input;

        while (computer.isRunning()) {
            System.out.print("sim:$ ");
            input = keyboard.nextLine();
            if (input.length() >= 2) {
                if (input.substring(1, 2).equals(" ")) {
                    switch (input.substring(0, 1)) {
                        case "A":
                            try {
                                computer.addProcess(Integer.parseInt(input.substring(2)));
                            } catch (NumberFormatException e) {
                                System.out.println(input.substring(2) + " is not a number.");
                            }
                            break;
                        case "d":
                            if (!computer.requestFile(input.substring(2)))
                                System.out.println("Disk does not exist, or there is no process in CPU.");
                            break;
                        case "D":
                            try {
                                if (!computer.diskDoneWithProcess(Integer.parseInt(input.substring(2))))
                                    System.out.println("Disk does not exist, or I/O for this disk is empty.");
                            } catch (NumberFormatException e) {
                                System.out.println(input.substring(2) + " is not a number.");
                            }
                            break;
                        case "m":
                            try {
                                if (!computer.loadPage(Integer.parseInt(input.substring(2))))
                                    System.out.println("Address is out of boundaries.");
                            } catch (NumberFormatException e) {
                                System.out.println(input.substring(2) + " is not a number.");
                            }
                            break;
                        case "S":
                            switch (input.substring(2, 3)) {
                                case "r":
                                    if (!computer.printReadyQueue())
                                        System.out.println("Ready-Queue is empty.");
                                    break;
                                case "i":
                                    computer.printDisksState();
                                    break;
                                case "m":
                                    if (!computer.printFrameTable())
                                        System.out.println("Memory is free.");
                                    break;
                                default:
                                    System.out.println("Input command not found!");
                                    break;
                            }
                            break;
                        default:
                            System.out.println("Input command not found!");
                            break;
                    }
                } else {
                    System.out.println("Wrong format input!");
                }
            } else if (input.length() == 1 && input.substring(0, 1).equals("T")) {
                System.out.println("Terminating the machine.");
                computer.terminateComputer();
            } else if (input.length() == 1 && input.substring(0, 1).equals("t")) {
                if (!computer.terminateProcess())
                    System.out.println("Ready-Queue is empty.");
            } else {
                System.out.println("Wrong format input!");
            }
        }
        System.out.println("Simulation ended.");
    }
}
